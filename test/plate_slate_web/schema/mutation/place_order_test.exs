defmodule PlateSlateWeb.Schema.Mutation.PlaceOrderTest do
  use PlateSlateWeb.ConnCase, async: true

  setup do
    PlateSlate.Seeds.run()
    :ok
  end

  @query """
  mutation ($input: PlaceOrderInput!) {
    placeOrder(input: $input) {
      errors { key message }
      order {
        items {
          quantity
          name
        }
        state
      }
    }
  }
  """

  test "placeOrder field creates an order" do
    order_input = %{
      "customerNumber" => 24,
      "items" => [
        %{"quantity" => 2, "menuItemId" => menu_item("Reuben").id}
      ]
    }

    user = Factory.create_user("employee")

    conn = build_conn() |> auth_user(user)
    conn = post(conn, "/api", query: @query, variables: %{input: order_input})

    assert json_response(conn, 200) == %{
             "data" => %{
               "placeOrder" => %{
                 "errors" => nil,
                 "order" => %{
                   "state" => "created",
                   "items" => [
                     %{"quantity" => 2, "name" => "Reuben"}
                   ]
                 }
               }
             }
           }
  end

  defp auth_user(conn, user) do
    token = PlateSlateWeb.Authentication.sign(%{role: user.role, id: user.id})
    put_req_header(conn, "authorization", "Bearer #{token}")
  end
end
